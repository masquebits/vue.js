<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Task;
class TaskController extends Controller
{

	// Retrieve all tasks.
    public function index(){
    	$tasks = Task::all();
    	return view("tasks.index", ["tasks" => $tasks] );
    }


    // Get new task form.
    public function create(){

    	return view("tasks.new");
    }

    // Store a task.

    public function store(Request $Request)
    {
    	$task = new Task;

    	$task->subject = $Request->get('subject');
    	$task->body = $Request->get('body');
    	$task->completed = 0;

    	$task->save();

    	return redirect('/');
    }
}
