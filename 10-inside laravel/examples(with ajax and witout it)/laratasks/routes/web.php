<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'TaskController@index')->name('tasks');

Route::get('task/new', 'TaskController@create')->name('newTask');

Route::post('task/new', 'TaskController@store')->name('storeTask');
