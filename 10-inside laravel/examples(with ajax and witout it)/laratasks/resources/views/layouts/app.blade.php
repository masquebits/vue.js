<!DOCTYPE html>
<head>
	<meta charset=utf-8 />
   <title>App Name - @yield("title")</title>
 	<link rel="stylesheet" type="text/css" media="screen"
 	 href="{{ URL::asset('css/bootstrap.min.css') }}" />


    <!-- Latest compiled and minified CSS -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    -->


	<!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Latest compiled and minified CSS -->
</head>
    <body>
      
        @yield("sidebar")
        <div class="container">
            @yield("content")
        </div>

         @yield("javascript")
 
    </body>
</html>