
@extends("layouts.app")

@section("title")
    Add new task
@endsection

@section("content")

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add Task </h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 content">
         
            {!! Form::open(['url' => 'task/new', 'method' => 'post']) !!}
            <div class="form-group">
                {!! Form::label('subject', 'task title', ['class' => 'item']) !!}
                {!!  Form::text('subject', '',['class' => 'form-control'])  !!}
               
                <div class="form-group">
                    <label for="pwd">Body:</label>
                     {!! Form::text('body', 'task description',['class' => 'form-control']) !!}
                </div>
               
                {!!  Form::submit('Save task') !!}
                {!! Form::close() !!}

            </div> 
        </div>
    </div>
@endsection
</div>