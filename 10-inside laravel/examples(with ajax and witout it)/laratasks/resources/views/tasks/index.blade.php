
@extends("layouts.app")
 
@section("title")
	All Tasks
@endsection
@section("content")
<div class="row">
	<div class="col-md-6 col-md-offset-3">

	<tasks></tasks>
		
	</div>
</div>

<template id="tasks-template">
	<h3>Tasks</h3>
	<ul>
		<li class="list-group-item" v-for="task in list">
			@{{ task.body }}
		</li>
	</ul>
</template>
@stop

@section("javascript")
			@parent

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>		
		 <script type="text/javascript" src="{{ URL::asset('js/vue.js') }}"></script>	 
        <script type="text/javascript" src="{{ URL::asset('js/main.js') }}"></script>
@endsection