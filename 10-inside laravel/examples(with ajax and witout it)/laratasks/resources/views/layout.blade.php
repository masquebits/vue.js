<!DOCTYPE html>
<head>
	<meta charset=utf-8 />
   <title>App Name - @yield('title')</title>
 	<link rel="stylesheet" type="text/css" media="screen"
 	 href="{{ URL::asset('css/bootstrap.min.css') }}" />


    <!-- Latest compiled and minified CSS -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    -->
	<link rel="stylesheet" type="text/css" media="screen" href="css/master.css" />

	<!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Latest compiled and minified CSS -->
</head>
    <body>
        @section('sidebar')
            This is the master sidebar.
        @show

        <div class="container">
            @yield('content')
        </div>

         @section('javascript')
            <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
         @endsection
    </body>
</html>